# gatsby-tinacms-gitlab-pages

![The gatsby-tinacms-gitlab-pages logo](./gatsby-tinacms-gitlab-pages.png)

A demo project that showcases [Gatsby](https://www.gatsbyjs.org/), [TinaCMS](https://tinacms.org/), and [GitLab Pages](https://about.gitlab.com/product/pages/).
